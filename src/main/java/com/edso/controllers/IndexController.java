package com.edso.controllers;

import com.edso.commands.AuthorCommand;
import com.edso.services.AuthorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@Controller
public class IndexController {
    private final AuthorService authorService;

    public IndexController(AuthorService authorService) {
        this.authorService = authorService;
    }

    @GetMapping
    @RequestMapping({"", "/", "index"})
    public String getIndexPage(Model model){
        model.addAttribute("authors", authorService.getAuthors());
        return "index";
    }

    @RequestMapping("author/add")
    public String addAuthorPage(Model model){
        model.addAttribute("author", new AuthorCommand());
        return "add_or_update";
    }

    @RequestMapping("author/{id}/update")
    public String updateAuthorPage(@PathVariable String id, Model model){
        log.debug("updating id: " + id);
        model.addAttribute("author", authorService.findById(Long.valueOf(id)));
        return "add_or_update";
    }

    @PostMapping("author")
    public String saveOrUpdate(@Valid  @ModelAttribute("author") AuthorCommand command, BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            bindingResult.getAllErrors().forEach(objectError -> {
                log.debug(objectError.toString());
            });

            return "redirect:/";
        }
        AuthorCommand savedCommand = authorService.saveAuthorCommand(command);
        return "redirect:/";
    }

    @GetMapping
    @RequestMapping("author/{id}/delete")
    public String deleteById(@PathVariable String id){
        log.debug("deleting id: " + id);
        authorService.deleteById(Long.valueOf(id));
        return "redirect:/";
    }

    public void updateAuthorName(Long id, String name){
        authorService.updateAuthorName(id, name);
    }

    public void updateAuthorNameByTransaction(Long id, String name){
        authorService.updateAuthorWithRollbackCustom(id, name);
    }

}
