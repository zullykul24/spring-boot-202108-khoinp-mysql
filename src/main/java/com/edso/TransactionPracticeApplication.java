package com.edso;

import com.edso.controllers.IndexController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class TransactionPracticeApplication {

    public static void main(String[] args){

        ApplicationContext ctx = SpringApplication.run(TransactionPracticeApplication.class, args);
        IndexController controller = ctx.getBean("indexController", IndexController.class);

//        controller.updateAuthorName(1L, "Nam");
//        controller.updateAuthorNameByTransaction(2L, "Transaction");
//        try {
//            controller.updateByHQL(3L, "HQL");
//        } catch (Exception e){
//            e.printStackTrace();
//        }
        //first author's name change from alex to Nam
        //second author's name change from james to Transaction
        //third author's name change from bobby to HQL



//        AuthorRepository authorRepository = ctx.getBean(AuthorRepository.class);
//        authorRepository.update(4L, "kicm");
//        System.out.println("List users:");
//        Collection<Author> list = authorRepository.findAllAuthors();
//        for(Author i: list){
//            System.out.println(i.toString());
//        }
    }

}
