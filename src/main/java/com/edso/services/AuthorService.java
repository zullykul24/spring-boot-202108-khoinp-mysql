package com.edso.services;

import com.edso.commands.AuthorCommand;
import com.edso.domain.Author;

import java.util.Optional;
import java.util.Set;

public interface AuthorService {
    Set<Author> getAuthors();
    void updateAuthorName(Long id, String name);
    void updateAuthorWithRollbackCustom(Long id, String name);
    AuthorCommand saveAuthorCommand(AuthorCommand command);
    void deleteById(Long id);
    Optional<Author> findById(Long id);
}
