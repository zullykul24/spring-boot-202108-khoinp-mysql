package com.edso.services;

import com.edso.commands.AuthorCommand;
import com.edso.converters.AuthorCommandToAuthor;
import com.edso.converters.AuthorToAuthorCommand;
import com.edso.domain.Author;
import com.edso.exceptions.NotFoundException;
import com.edso.repositories.AuthorRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class AuthorServiceImpl implements AuthorService {

    private final AuthorRepository authorRepository;

    private final AuthorCommandToAuthor authorCommandToAuthor;
    private final AuthorToAuthorCommand authorToAuthorCommand;

    public AuthorServiceImpl(AuthorRepository authorRepository, AuthorCommandToAuthor authorCommandToAuthor,
                             AuthorToAuthorCommand authorToAuthorCommand) {
        this.authorRepository = authorRepository;
        this.authorCommandToAuthor = authorCommandToAuthor;
        this.authorToAuthorCommand = authorToAuthorCommand;
    }

    @Override
    public Set<Author> getAuthors() {
        Set<Author> authors = new HashSet<>();
        authorRepository.findAll().iterator().forEachRemaining(authors::add);
        return authors;
    }

    @Override
    @Transactional
    public void updateAuthorName(Long id, String name) {
        Author author = authorRepository.findById(id).get();
        author.setName(name);
    }

    @Override
    @Transactional(rollbackFor = Exception.class, noRollbackFor = EntityNotFoundException.class)
    public void updateAuthorWithRollbackCustom(Long id, String name) {
        Author author = authorRepository.findById(id).orElse(null);
        author.setName(name);
    }

    @Override
    @Transactional
    public AuthorCommand saveAuthorCommand(AuthorCommand command) {
        Author detachedAuthor = authorCommandToAuthor.convert(command);
        Author savedAuthor = authorRepository.save(detachedAuthor);
        return authorToAuthorCommand.convert(savedAuthor);
    }

    @Override
    public void deleteById(Long id) {
        authorRepository.deleteById(id);
    }

    @Override
    public Optional<Author> findById(Long id) {
        Optional<Author> authorOptional =  authorRepository.findById(id);
        if(!authorOptional.isPresent()){
            throw new NotFoundException("Author id not found, with id = " + id);
        }

        return authorOptional;
    }

}
