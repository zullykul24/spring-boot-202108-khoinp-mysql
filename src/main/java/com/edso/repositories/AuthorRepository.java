package com.edso.repositories;

import com.edso.domain.Author;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

public interface AuthorRepository extends CrudRepository<Author, Long> {
    @Override
    Optional<Author> findById(Long id);


    //native query
    @Transactional
    @Query(value = "SELECT * FROM Author author", nativeQuery = true)
    Collection<Author> findAllAuthors();

    @Transactional
    @Modifying
    @Query(value = "UPDATE Author SET name =?2 where id=?1")
    void update(Long id,String name);
}
