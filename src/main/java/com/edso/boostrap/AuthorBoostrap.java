package com.edso.boostrap;

import com.edso.domain.Author;
import com.edso.repositories.AuthorRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Profile("default")
@Component
public class AuthorBoostrap implements ApplicationListener<ContextRefreshedEvent> {
    private final AuthorRepository authorRepository;

    public AuthorBoostrap(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        authorRepository.saveAll(getAuthor());
    }

    private List<Author> getAuthor(){
        List<Author> authors = new ArrayList<>();
        authors.add(new Author("alex"));
        authors.add(new Author("james"));
        authors.add(new Author("bobby"));
        authors.add(new Author("jack"));

        return authors;
    }
}
