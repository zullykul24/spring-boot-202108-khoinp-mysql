package com.edso.boostrap;

import com.edso.domain.Author;
import com.edso.repositories.AuthorRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@Profile({"dev", "prod"})
public class BootStrapMySQL implements ApplicationListener<ContextRefreshedEvent> {

    private final AuthorRepository authorRepository;


    public BootStrapMySQL(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {

        if (authorRepository.count() == 0L){
            log.debug("Loading data");
            loadData();
        }

    }

    private void loadData(){
        Author author1 = new Author();
        author1.setName("Khoi");
        authorRepository.save(author1);

        Author author2 = new Author();
        author2.setName("Cong");
        authorRepository.save(author2);

        Author author3 = new Author();
        author3.setName("Lan");
        authorRepository.save(author3);
    }

}